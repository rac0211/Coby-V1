#!/bin/bash
  
  #INPUT="../data/yedGen"
  #OUTPUT="../mapping/mapping.obda"
  #EXTENSION=".graphml"

  cd $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

  get_abs_filename() {
    # $1 : relative filename
    echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
  }
  
  INPUT=${1:-"../data/yedGen"} 
  OUTPUT=${2:-"../mapping/mapping.obda"}
  EXTENSION=${3:-".graphml"}
  
  CSV_FILE=${4:-"-csv $( get_abs_filename "../data/csv-files/ola.csv")" }
  PRF=${5:-"-prf $( get_abs_filename "../data/csv-files/ola.properties")" }
  JS=${6:-"-js $( get_abs_filename "../data/csv-files/ola-scripts.js")" }
  INCLUDE_GRAPH_VARIAVLES=${7:-""}  # -ig parameter
  
  EXIT() {
     parent_script=`ps -ocommand= -p $PPID | awk -F/ '{print $NF}' | awk '{print $1}'`
     if [ $parent_script = "bash" ] ; then
         echo; echo -e " \e[90m exited by : $0 \e[39m " ; echo
         exit 2
     else
         echo ; echo -e " \e[90m exited by : $0 \e[39m " ; echo
         kill -9 `ps --pid $$ -oppid=`;
         exit 2
     fi
  } 
  
  tput setaf 2
  echo 
  echo -e " ######################################### "
  echo -e " ######## Info Generation ################ "
  echo -e " ----------------------------------------- "
  echo -e "\e[90m$0         \e[32m                    "
  echo
  echo -e " ##  INPUT     : $INPUT                    "
  echo -e " ##  EXTENTION : $EXTENSION                "
  echo
  echo -e " ##  CSV_FILE  : $CSV_FILE                 "
  echo -e " ##  Prop File : $PRF                      "
  echo -e " ##  JS File   : $JS                       "
  echo -e " ##  GRAPH_VAR : $INCLUDE_GRAPH_VARIAVLES  "
  echo
  echo -e " ##  OUTPUT    : $OUTPUT                   "
  echo
  echo -e " ######################################### "
  echo 
  sleep 1
  tput setaf 7

  if [ ! -d $INPUT ] ; then
     echo -e "\e[91m $INPUT is not a valid Directory ! \e[39m "
     EXIT
  fi

  echo -e "\e[90m Starting Generation... \e[39m "
  echo
  
  # TREAT CSV
  java -cp ../libs/yedGen.jar entypoint.Main -d $INPUT                     \
                                             -out $OUTPUT                  \
                                             -ext $EXTENSION               \
                                                  $CSV_FILE                \
                                                  $PRF                     \
                                                  $JS                      \
                                                  $INCLUDE_GRAPH_VARIAVLES
<<COMMENT

# TREAT Only variables enumerated in Graph
  java -cp ../libs/yedGen.jar entypoint.Main -d $INPUT       \
                                             -out $OUTPUT    \
                                             -ext $EXTENSION \
                                             -ig 
COMMENT

  echo -e "\e[36m Mapping generated in : $OUTPUT \e[39m "
  echo
  
