# Coby pipeline 
<h6>Corese Ontop BlazeGraph yedGen - Standalone </h6>

 **master_pipeline Parameters :**
 
-    `$1 : IP_HOST ( or Hostname )`

-    `$2 : NAME_SPACE `

-    `$3 : Read_Write_Port `

-    `$4 : Read_Only_Port `

-    `$5 : DATA_BASE { [postgresql] - mysql }`

**Example :**

   1 - Using Postgresql database ( default installation ) :

```
    ❯   ./pipeline_fullGraph.sh \
          localhost             \
          ola                   \
          6981                  \
          6982
```
    
   2 - Using Mysql database :

```
    ❯   ./pipeline_fullGraph.sh \
          localhost             \
          ola                   \
          6981                  \
          6982                  \
          mysql     
```
     
**Dependencies :**

-    [https://github.com/rac021/yedGen]( https://github.com/rac021/yedGen)

-    [https://github.com/rac021/ontop-matarializer]( https://github.com/rac021/ontop-matarializer)
   
-    [https://github.com/rac021/CoreseInfer]( https://github.com/rac021/CoreseInfer)

-    [https://github.com/rac021/blazegraph_libs]( https://github.com/rac021/blazegraph_libs)
   

**Requirements :**

-    `JAVA 8`
    
-    `MAVEN`
   
-    `CURL `
    
-    `[ Postgres ] / MySql`

-    `Docker 1.10 and +  ( Optionnal ) ` 


----------------------------------------------------

### **Pipeline description**
 
   As needs, scripts [pipeline_fullGraph.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/pipeline_fullGraph.sh), [pipeline_graphChunks.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/pipeline_graphChunks.sh) , [pipeline_patternGraphChunks.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/pipeline_patternGraphChunks.sh) *orchestrate the pipeline*
           
#### * **Scripts ( folder scripts )**

*  **[00_install_libs.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/00_install_libs.sh)**

     - Take 0, 1 or 2 arguments
     - **$1 DATA_BASE :** Postgresql / mySql 
     - **$2 TYPE_INSTALL :** demo / graphChunks / patternGraphChunks
     - Create needed folders
     - Default database : Postgresql. Supported database : mySql
     - Install [yedGen]( https://github.com/rac021/yedGen) : **libs/yedGen.jar**
     - Copy yedGen documentation + examples to **libs/Docs**
     - Install [ontop-matarializer]( https://github.com/rac021/ontop-matarializer) : **libs/Ontop-Materializer.jar**
     - Copy Ontop-Materializer documentation + examples to **libs/ontop**
     - Install [CoreseInfer]( https://github.com/rac021/CoreseInfer) : **libs/CoreseInfer.jar**
     - Copy CoreseInfer documentation + examples to **libs/corese**
     - Install [Blazegraph 2.1.1]( https://github.com/rac021/blazegraph_libs) : **libs/Blazegraph/Blazegraph_2.1.jar**     

*  **[01_build_config.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/01_build_config.sh)** [ Host - nameSpace - ReadWritePort - ReadOnlyPort ]

     - Take exactly Four arguments
     - **$1 IP-Host :** IP of the endPoint
     - **$2 nameSpace :** Name Space that will be used by blazeGraph
     - **$3 ReadWritePort :** Port Number that will be used by blazeGraph in ReadWrite Mode
     - **$4 ReadOnlyPort :** Port Number that will be used by blazeGraph in ReadOnly Mode
     - Check if **libs/Blazegraph/data/blazegraph.jnl** exists and *remove* it
     - Create blazegraph database in **libs/Blazegraph/data/blazegraph.jnl**
     - Create appropriate nameSpace using **scripts/conf/blazegraph**
     - Write in **scripts/conf/BLZ_INFO_INSTALL** >> **../libs/Blazegraph/Blazegraph_2.1.jar**
     - Write in **scripts/conf/nanoEndpoint** >> **Host - nameSpace - ReadWritePort - ReadOnlyPort**
     - Refer to script **03_nano_start_stop.sh** for *ReadWritePort* and *ReadOnlyPort*
     
*  **[02_docker_nginx.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/02_docker_nginx.sh)** 

     - Used to solves some proxy problems
     - Take 1 or * ( with max arguments = 9 )
     - if **$1 = start** : start nginx docker container
     - if **$1 = stop** : stop nginx docker container
     - **$2 DEFAULT_IP :** IP of the Nginx container. Optionnal. Default **192.168.56.110**
     - **$3 SUBNET :** Optionnal. Default : **mynet123**
     - **$4 SUBNET_RANGE :** Optionnal. Default : **192.168.56.250/24**
     - **$5 DEFAULT_PORT :** Nginx Port. Optionnal. Default : **80**
     - **$6 LOCAL_IP :** IP OF THE LocalHost. Optionnal. Default **127.0.0.1**
     - **$7 IMAGE_NAME :** Name of the Nginx Container. Optionnal. Default : **nginx-ecoinformatics**
     - **$8 HOST :** Host that will be add to **/etc/hosts** in the Host Machine. Optionnal. Default : **ecoinformatics.org**
     - **$9 FOLDER_DOCKER_FILE :** localtion of the wb files. Optionnal. Default : **docker_nginx_server**
     - The script will remove the container based on images **IMAGE_NAME** if already exists
     - Build and run **nginx-ecoinformatics** docker image Based on the  [Dockerfile]( https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/docker/Dockerfile) 
     - Ontologies accessible locally
     - If start : add **"127.0.0.1 ecoinformatics.org"** to **/etc/hosts**
     - If stop : remove **"127.0.0.1 ecoinformatics.org"** from **/etc/hosts**
     
*  **[03_nano_start_stop.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/03_nano_start_stop.sh)** [ start / stop - ro / rw ]

     - Take 1 or * ( with max arguments = 4 )
     - **$1 start * stop :**
     - **$2 rw / ro :** rw = readWrite Mode. ro = readOnly Mode. Optionnal. Default **ro**
     - **$3 XMS :** Optionnal. Default **-Xms3g**
     - **$4 XMX :** Optionnal. Default **-Xms5g**
     - If **start** with **rw** : Endpoint launch using **ReadWritePort**
     - If **start** with **ro** : Endpoint launch using **ReadOnlyPort**
     - Purpose : write locally, read from anywhere
     - Relies on **scripts/conf/BLZ_INFO_INSTALL** and **scripts/conf/nanoEndpoint** files
     
*  **[04_gen_mapping.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/04_gen_mapping.sh)** 

     - Take 0 or * ( with max arguments = 3 )
     - **$1 INPUT :** path of graph files. Optionnal.Default **data/yedGen/**
     - **$2 OUTPUT :** obda file(s). Optionnal. Default **mapping/mapping.obda**
     - **$3 EXTENSION :** Extension of the graph files that wil be processed. Optionnal. Default **.graphml**
     - Refer to [https://github.com/rac021/yedGen]( https://github.com/rac021/yedGen)
   
*  **[05_ontop_gen_triples.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/05_ontop_gen_triples.sh)**

     - Take 0 or * ( with max arguments = 7 )
     - **$1 OWL :** Path of the Ontology(ies). Optionnal. Default  **mapping/ontology.owl**
     - **$2 OBDA :** Path of the obda mapping file. Optionnal. Default **mapping/mapping.obda**
     - **$3 OUTPUT :** Path output data. Optionnal. Default **data/ontop/ontopMaterializedTriples.ttl**
     - **$4 QUERY :** Query that will be executed. Optionnal. Default **SELECT ?S ?P ?O { ?S ?P ?O }**
     - **$5 TTL :** if **-ttl** then, enable **turtle format**, else xml. Optionnal. Default  **-ttl**
     - **$6 XMS :** Optionnal. Default **-Xms2048M**
     - **$7 XMX :** Optionnal. Default **-Xms2048M**
     - Refer to [https://github.com/rac021/ontop-matarializer]( https://github.com/rac021/ontop-matarializer)
     
*  **[06_corese_infer.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/06_corese_infer.sh)** 
  
     - Used to infer triples
     - Take 0 or * ( with max arguments = 8 )
     - **$1 OWL :** Path of the Ontology(ies). Optionnal. Default  **mapping/ontology.owl**
     - **$2 TTL :** Path of the triples file. Optionnal. Default **data/ontop/ontopMaterializedTriples.ttl**
     - **$3 QUERY :** Query that will be executed. Optionnal. Default **SELECT ?S ?P ?O { ?S ?P ?O }**
     - **$4 OUTPUT :** Directory where output results.
     - **$5 f :** Fragments. Number of triples per file. if = 0 then unlimited. Optionnal. Default **100000**
     - **$6 F :** Output Format. **ttl**. Supported XML, CSV. Optionnal. Default : **ttl**
     - **$7 XMS :**  Optionnal. Default **-Xms2048M**
     - **$8 XMX :**  Optionnal. Default **-Xms2048M**
     - Refer to [https://github.com/rac021/CoreseInfer]( https://github.com/rac021/CoreseInfer)
     
     
*  **[07_load_data.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/07_load_data.sh)** 

     - Used to load data on the blazeGraph endpoint.
     - Take 0 or 1 argument
     - **$1 DATA_DIR :** Directory where files to load are located. Optionnal. Default **data/corese**
     - Relies on **scripts/conf/nanoEndpoint** file
     - inform if endPoint not reachable

*  **[08_portal_query.sh]( https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/08_portal_query.sh)** 

     - Create Synthesis for portal
     - Take 1 or 4 arguments
     - if One argument **$1 OUT :** Path of the output result file
     - if Four arguments **$1 IP :** IP of the blazeGraph endPoint.
     - **$2 PORT :** Port of the blazeGraph endPoint.
     - **$3 NAMESPACE :** : NameSpace that will be queried
     - **$4 OUT :** Path of the output result file
     - Relies on **scripts/conf/nanoEndpoint** file if just one argument ( OUT ) passed to this script
     - inform if endPoint not reachable

----------------------------------------------------

## [Summary](https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/docs/01_pipeline_scripts.graphml) 

![pipeline_01](https://cloud.githubusercontent.com/assets/7684497/17776064/4f954b40-655b-11e6-9d23-7f02c64c6ea9.png)


----------------------------------------------------

## [Workflow Ontop - Corese - triples generation](https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/docs/02_workflow_ontop_corese.graphml)

![archi_pipeline](https://cloud.githubusercontent.com/assets/7684497/17937287/d79f57fe-6a21-11e6-9e34-7a8f89c6461f.png)

----------------------------------------------------

## [Archi SI AnaEE](https://github.com/rac021/AnaEE_StandAlone/blob/master/scripts/docs/03_archi_SI_AnaEE.graphml)

![archi_si_anaee](https://cloud.githubusercontent.com/assets/7684497/17937165/81ddd214-6a21-11e6-983c-b17080a7d010.png)

